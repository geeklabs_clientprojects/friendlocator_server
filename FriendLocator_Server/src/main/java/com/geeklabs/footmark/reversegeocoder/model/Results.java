package com.geeklabs.footmark.reversegeocoder.model;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;

public class Results {
	private List<String> types = new ArrayList<String>();
	private String formatted_address;
	private List<AddressComponent> address_components = new ArrayList<AddressComponent>();
	private Geometry geometry;
	@JsonIgnore
	private boolean partialMatch;
	
	private String place_id;

	public List<String> getTypes() {
		return types;
	}

	public void setTypes(List<String> types) {
		this.types = types;
	}

	public void setFormatted_address(String formatted_address) {
		this.formatted_address = formatted_address;
	}

	public String getFormatted_address() {
		return formatted_address;
	}

	public void setAddress_components(
			List<AddressComponent> address_components) {
		this.address_components = address_components;
	}

	public List<AddressComponent> getAddress_components() {
		return address_components;
	}

	public Geometry getGeometry() {
		return geometry;
	}

	public void setGeometry(Geometry geometry) {
		this.geometry = geometry;
	}

	public boolean isPartialMatch() {
		return partialMatch;
	}

	public void setPartialMatch(boolean partialMatch) {
		this.partialMatch = partialMatch;
	}

	public String getPlace_id() {
		return place_id;
	}

	public void setPlace_id(String place_id) {
		this.place_id = place_id;
	}
}

