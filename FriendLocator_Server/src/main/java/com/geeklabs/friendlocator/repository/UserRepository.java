package com.geeklabs.friendlocator.repository;

import com.geeklabs.friendlocator.domain.User;
import com.geeklabs.friendlocator.repository.objectify.ObjectifyCRUDRepository;

public interface UserRepository extends ObjectifyCRUDRepository<User>{

	User getUserByEmail(String email);

}
