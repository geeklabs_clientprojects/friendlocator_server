package com.geeklabs.friendlocator.repository;

import java.util.List;

import com.geeklabs.friendlocator.domain.Friend;
import com.geeklabs.friendlocator.repository.objectify.ObjectifyCRUDRepository;

public interface FriendsRepository extends ObjectifyCRUDRepository<Friend> {

	List<Friend> getMyFriends(long userId);
	
	List<Friend> getMyFriendRequests(long userId);

	Friend unFriend(long userId, long friendId);

	List<Friend> getMyFriendsList(long parseLong);
	
	List<Friend> getMyFriendRequestsList(long parseLong);

	boolean isAlreadyFriendsBySender(Long senderId, Long recieverId);

	boolean isAlreadyFriendsByReciever(Long recieverId, Long senderId);

	boolean isAlreadyFriendsBySenderAcceptStatus(Long senderId, Long recieverId);

	boolean isAlreadyFriendsByRecieverAcceptStatus(Long recieverId, Long senderId);
}
