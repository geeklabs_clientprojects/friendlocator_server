package com.geeklabs.friendlocator.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.geeklabs.friendlocator.domain.User;
import com.geeklabs.friendlocator.repository.UserRepository;
import com.geeklabs.friendlocator.repository.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

public class UserRepositoryImpl extends  AbstractObjectifyCRUDRepository<User> implements UserRepository {

	@Autowired
	private Objectify objectify;
	@Override
	protected Objectify getObjectify() {
		return objectify;
	}
	
	public UserRepositoryImpl() {
		super(User.class);
	}
	
	@Override
	public User getUserByEmail(String email) {
		return objectify.load()
				  		.type(User.class)
				  		.filter("email", email)
						.first()
						.now();
	}
}
