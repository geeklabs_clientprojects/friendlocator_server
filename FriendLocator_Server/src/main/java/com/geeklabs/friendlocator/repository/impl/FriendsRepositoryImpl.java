package com.geeklabs.friendlocator.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.geeklabs.friendlocator.domain.Friend;
import com.geeklabs.friendlocator.repository.FriendsRepository;
import com.geeklabs.friendlocator.repository.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

public class FriendsRepositoryImpl extends AbstractObjectifyCRUDRepository<Friend> implements FriendsRepository {

	@Autowired
	private Objectify objectify;
	
	@Override
	protected Objectify getObjectify() {
		return objectify;
	}
	
	public FriendsRepositoryImpl() {
		super(Friend.class);
	}

	@Override
	public List<Friend> getMyFriends(long userId) {
		return objectify.load()
						.type(Friend.class)
						.filter("sender_Id", userId)
						.filter("isAccepted", true)
						.filter("isRejected", false)
						.list();
	}
	
	@Override
	public List<Friend> getMyFriendsList(long userId) {

		return objectify.load()
				.type(Friend.class)
				.filter("friend_Id", userId)
				.filter("isAccepted", true)
				.filter("isRejected", false)
				.list();
	}
	
	@Override
	public List<Friend> getMyFriendRequests(long userId) {
		return objectify.load()
						.type(Friend.class)
						.filter("sender_Id", userId)
						.filter("isAccepted", false)
						.filter("isRejected", false)
						.list();
	}
	
	@Override
	public List<Friend> getMyFriendRequestsList(long userId) {

		return objectify.load()
				.type(Friend.class)
				.filter("friend_Id", userId)
				.filter("isAccepted", false)
				.filter("isRejected", false)
				.list();
	}
	
	@Override
	public Friend unFriend(long userId, long friendId) {
		return objectify.load()
						.type(Friend.class)
						.filter("sender_Id", userId)
						.filter("friend_Id", friendId)
						.first()
						.now();
	}
	
	@Override
	public boolean isAlreadyFriendsBySender(Long senderId, Long recieverId) {
				Friend friend = objectify.load()
				.type(Friend.class)
				.filter("sender_Id", senderId)
				.filter("friend_Id", recieverId)
				.filter("isAccepted", true)
				.filter("isRejected", false)
				.first()
				.now();
				
				if (friend != null) {
					return true;
				}
		return false;
	}
	@Override
	public boolean isAlreadyFriendsByReciever(Long recieverId, Long senderId) {
		Friend friend = objectify.load()
				.type(Friend.class)
				.filter("sender_Id", recieverId)
				.filter("friend_Id", senderId)
				.filter("isAccepted", true)
				.filter("isRejected", false)
				.first()
				.now();
		
		if (friend != null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isAlreadyFriendsBySenderAcceptStatus(Long senderId, Long recieverId) {
		Friend friend = objectify.load()
		.type(Friend.class)
		.filter("sender_Id", senderId)
		.filter("friend_Id", recieverId)
		.filter("isAccepted", false)
		.filter("isRejected", false)
		.first()
		.now();
		
		if (friend != null) {
			return true;
		}
return false;
}

	@Override
	public boolean isAlreadyFriendsByRecieverAcceptStatus(Long recieverId, Long senderId) {
		Friend friend = objectify.load()
				.type(Friend.class)
				.filter("sender_Id", recieverId)
				.filter("friend_Id", senderId)
				.filter("isAccepted", false)
				.filter("isRejected", false)
				.first()
				.now();
		
		if (friend != null) {
			return true;
		}
		return false;
	}
}
