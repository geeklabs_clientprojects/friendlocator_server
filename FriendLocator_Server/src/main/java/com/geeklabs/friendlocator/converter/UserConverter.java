package com.geeklabs.friendlocator.converter;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;

import com.geeklabs.friendlocator.domain.User;
import com.geeklabs.friendlocator.dto.UserDto;

public class UserConverter {

	public static List<UserDto> convertUserListIntoUserDtoList(List<User> users, DozerBeanMapper dozerBeanMapper) {
		
		List<UserDto> userDtos = new ArrayList<>();
		for (User user : users) {
			UserDto userDto = dozerBeanMapper.map(user, UserDto.class);
			userDtos.add(userDto);
		}
		return userDtos;
	}

	public static UserDto convertUserIntoUserDto(User user, DozerBeanMapper dozerBeanMapper) {
		return dozerBeanMapper.map(user, UserDto.class);
	}

}
