package com.geeklabs.friendlocator.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.friendlocator.dto.UserDto;
import com.geeklabs.friendlocator.service.UserService;
import com.geeklabs.friendlocator.util.ResponseStatus;

@Controller
@RequestMapping(value = "/user")
public class UserController {

	private UserService userService;

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@RequestMapping(value = "/signin", method = RequestMethod.POST)
	@ResponseBody
	public ResponseStatus register(@RequestBody UserDto userDto) {
		return userService.registerUser(userDto);
	}

	@RequestMapping(value = "/signout/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseStatus signOut(@PathVariable String id) {
		return userService.signOut(id);
	}

	@RequestMapping(value = "/get", method = RequestMethod.GET)
	@ResponseBody
	public List<UserDto> getAllUser() {
		return userService.getAllUsers();
	}
}
