package com.geeklabs.friendlocator.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.friendlocator.dto.TrackDto;
import com.geeklabs.friendlocator.service.TrackService;
import com.geeklabs.friendlocator.util.ResponseStatus;

@RequestMapping(value = "track")
public class TrackController {

	private TrackService trackService;

	public void setTrackService(TrackService trackService) {
		this.trackService = trackService;
	}

	@RequestMapping(value = "/save/{userId}")
	public @ResponseBody ResponseStatus saveTrack(@PathVariable String userId, @RequestBody TrackDto trackDto) {
		if (userId != null) {
			return trackService.saveTrack(userId, trackDto);
		} else {
			return null;
		}
	}
}
