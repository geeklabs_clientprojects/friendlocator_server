package com.geeklabs.friendlocator.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.friendlocator.dto.AddFriendDto;
import com.geeklabs.friendlocator.service.AddFriendService;
import com.geeklabs.friendlocator.util.ResponseStatus;

@Controller
@RequestMapping(value = "af")
public class AddFriendController {

	private AddFriendService addFriendService;
	
	public void setAddFriendService(AddFriendService addFriendService) {
		this.addFriendService = addFriendService;
	}
	
	@RequestMapping(value = "/add/{id}", method = RequestMethod.POST)
	public @ResponseBody ResponseStatus addFriend(@RequestBody AddFriendDto addFriendDto, @PathVariable String id) {
		return addFriendService.addFriendRequest(addFriendDto, id);
	}
}
