package com.geeklabs.friendlocator.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.friendlocator.dto.AddFriendDto;
import com.geeklabs.friendlocator.dto.UserDto;
import com.geeklabs.friendlocator.service.FriendService;
import com.geeklabs.friendlocator.util.ResponseStatus;

@RequestMapping(value = "friend")
public class FriendController {
	
	private FriendService friendService;
	
	public void setFriendService(FriendService friendService) {
		this.friendService = friendService;
	}
	
	@RequestMapping(value = "getFriends/{userId}", method = RequestMethod.GET)
	public @ResponseBody List<UserDto> getMyFriends(@PathVariable String userId) {
		return friendService.getMyFriends(userId);
	}
	
	@RequestMapping(value = "unFriend/{userId}/{friendId}", method = RequestMethod.GET)
	public @ResponseBody ResponseStatus unFriend(@PathVariable String userId, @PathVariable String friendId) {
		return friendService.unFriend(userId, friendId);
	}
	
	@RequestMapping(value = "findFriend/{friendId}", method = RequestMethod.GET)
	public @ResponseBody UserDto findFriendLocation(@PathVariable String friendId) {
		return friendService.findFriendLocation(friendId);
		
	}
	
	@RequestMapping(value = "friendReq/{id}", method = RequestMethod.GET)
	public @ResponseBody List<UserDto> getFriendReqs(@PathVariable String id) {
		return friendService.getFriendReqs(id);
		
	}
	
	@RequestMapping(value = "update", method = RequestMethod.POST)
	public @ResponseBody ResponseStatus getFriendReqStatus(@RequestBody AddFriendDto addFriendDto) {
		return friendService.setFriendRequestStatus(addFriendDto);
		
	}
}
