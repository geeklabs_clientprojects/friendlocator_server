package com.geeklabs.friendlocator.domain;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Friend {

	@Id
	private Long id;

	@Index
	private long sender_Id;

	@Index
	private long friend_Id;

	@Index
	private boolean isAccepted;
	
	@Index
	private boolean isRejected;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getSender_Id() {
		return sender_Id;
	}

	public void setSender_Id(long sender_Id) {
		this.sender_Id = sender_Id;
	}

	public long getFriend_Id() {
		return friend_Id;
	}

	public void setFriend_Id(long friend_Id) {
		this.friend_Id = friend_Id;
	}

	public boolean isAccepted() {
		return isAccepted;
	}

	public void setAccepted(boolean isAccepted) {
		this.isAccepted = isAccepted;
	}
	
	public void setRejected(boolean isRejected) {
		this.isRejected = isRejected;
	}
	
	public boolean isRejected() {
		return isRejected;
	}
}
