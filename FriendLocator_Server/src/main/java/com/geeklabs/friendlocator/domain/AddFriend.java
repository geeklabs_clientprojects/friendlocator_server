package com.geeklabs.friendlocator.domain;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class AddFriend {

	public static Key<AddFriend> key(long id) {
		return Key.create(AddFriend.class, id);

	}

	@Id
	private Long id;
	@Index
	private String emailId;
	private long friend_id;
	private long sender_Id;
	private String status;
	@Index
	private Key<AddFriend> addFriend;
	
	private boolean isAccepted = false;
	private boolean isRejected = false;

	public Key<AddFriend> getAddFriend() {
		return addFriend;
	}

	public void setAddFriend(Key<AddFriend> addFriend) {
		this.addFriend = addFriend;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public long getFriend_id() {
		return friend_id;
	}

	public void setFriend_id(long friend_id) {
		this.friend_id = friend_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getSender_Id() {
		return sender_Id;
	}

	public void setSender_Id(long sender_Id) {
		this.sender_Id = sender_Id;
	}

	public boolean isAccepted() {
		return isAccepted;
	}

	public void setAccepted(boolean isAccepted) {
		this.isAccepted = isAccepted;
	}

	public boolean isRejected() {
		return isRejected;
	}

	public void setRejected(boolean isRejected) {
		this.isRejected = isRejected;
	}

}
