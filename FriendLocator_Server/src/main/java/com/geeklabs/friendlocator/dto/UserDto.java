package com.geeklabs.friendlocator.dto;

import java.util.Date;

public class UserDto {

	private Long id;
	private String accessToken;
	private String email;
	private String name;
	private boolean isSignIn;
	private Date registeredDate;

	private double latitude;
	private double longitude;

	private String address;

	private String country;
	private String state;
	private String city;

	private Date trackTime;
	private long friendId;
	private String picUrl;

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public void setFriendId(long friendId) {
		this.friendId = friendId;
	}

	public long getFriendId() {
		return friendId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isSignIn() {
		return isSignIn;
	}

	public void setSignIn(boolean isSignIn) {
		this.isSignIn = isSignIn;
	}

	public Date getRegisteredDate() {
		return registeredDate;
	}

	public void setRegisteredDate(Date registeredDate) {
		this.registeredDate = registeredDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setTrackTime(Date trackTime) {
		this.trackTime = trackTime;
	}

	public Date getTrackTime() {
		return trackTime;
	}
}
