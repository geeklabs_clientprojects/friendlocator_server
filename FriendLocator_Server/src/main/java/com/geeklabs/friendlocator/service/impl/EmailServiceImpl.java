package com.geeklabs.friendlocator.service.impl;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.MimeMessageHelper;

import com.geeklabs.friendlocator.dto.UserDto;
import com.geeklabs.friendlocator.service.EmailService;

public class EmailServiceImpl implements EmailService {

	private static final String FROM_EMAIL = "geeklabsapps@gmail.com";

	@Override
	public void sendWelcomeMail(UserDto userDto) {
		send(userDto.getEmail(),
				"Welcome to Shyra app from Geek Labs",
				"Hai " + userDto.getName() + ", Thank you for installing Shyra app. "
						+ "This application can track your friends location details without calling or messaging him which works on GPS and INTERNET. Comment and rate our app at Google Play Store https://play.google.com/store/apps/details?id=com.geeklabs.friendlocator . For more info about us visit http://www.geeklabs.co.in/index.html ");
	}

	@Override
	public void send(String toEmail, String subject, String body) {
		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);

		MimeMessage msg = new MimeMessage(session);
		final MimeMessageHelper message = new MimeMessageHelper(msg);

		try {
			message.setFrom(new InternetAddress(FROM_EMAIL));
			InternetAddress to = new InternetAddress(toEmail);
			message.setTo(to);
			message.setSubject(subject);
			message.setText(body, true);

			Transport.send(message.getMimeMessage());
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

}
