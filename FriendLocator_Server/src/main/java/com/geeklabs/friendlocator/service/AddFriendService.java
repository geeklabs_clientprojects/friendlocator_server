package com.geeklabs.friendlocator.service;

import com.geeklabs.friendlocator.dto.AddFriendDto;
import com.geeklabs.friendlocator.util.ResponseStatus;

public interface AddFriendService {

	ResponseStatus addFriendRequest(AddFriendDto addFriendDto, String id);

}
