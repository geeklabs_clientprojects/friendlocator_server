package com.geeklabs.friendlocator.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;

import com.geeklabs.footmark.reversegeocoder.model.AddressComponent;
import com.geeklabs.footmark.reversegeocoder.model.GeoCodeResponse;
import com.geeklabs.footmark.reversegeocoder.model.Results;
import com.geeklabs.friendlocator.converter.UserConverter;
import com.geeklabs.friendlocator.domain.Friend;
import com.geeklabs.friendlocator.domain.User;
import com.geeklabs.friendlocator.dto.AddFriendDto;
import com.geeklabs.friendlocator.dto.UserDto;
import com.geeklabs.friendlocator.repository.FriendsRepository;
import com.geeklabs.friendlocator.service.FriendService;
import com.geeklabs.friendlocator.service.UserService;
import com.geeklabs.friendlocator.util.ResponseStatus;

public class FriendsServiceImpl implements FriendService, InitializingBean {

	private FriendsRepository friendsRepository;
	private UserService userService;
	private DozerBeanMapper dozerBeanMapper;

	public void setFriendsRepository(FriendsRepository friendsRepository) {
		this.friendsRepository = friendsRepository;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public void setDozerBeanMapper(DozerBeanMapper dozerBeanMapper) {
		this.dozerBeanMapper = dozerBeanMapper;
	}

	@Override
	@Transactional
	public void save(Friend friend) {
		friendsRepository.save(friend);
	}

	@Override
	@Transactional(readOnly = true)
	public List<UserDto> getMyFriends(String userId) {
		
		List<Friend> myFriendsSenderList = friendsRepository.getMyFriends(Long.parseLong(userId));
		List<Friend> myFriendsAcceptedList = getMyFriendList(userId);
		
		List<User> users = new ArrayList<User>();
		
		if (!myFriendsSenderList.isEmpty()) {
			users.addAll(getUsersFromFriendsList(myFriendsSenderList));
		} 
		if(!myFriendsAcceptedList.isEmpty())  {
			users.addAll(getUsersFromListOfFriends(myFriendsAcceptedList));
		}
		
		return UserConverter.convertUserListIntoUserDtoList(users, dozerBeanMapper);
	}

	private List<Friend> getMyFriendList(String userId) {
		return friendsRepository.getMyFriendsList(Long.parseLong(userId));
	}

	@Override
	@Transactional(readOnly = true)
	public List<UserDto> getFriendReqs(String userId) {
		List<Friend> myFriendsAcceptedList = getMyFriendRequestsList(userId);
		
		List<User> users = new ArrayList<User>();
		
		if(!myFriendsAcceptedList.isEmpty())  {
			users.addAll(getUsersFromListOfFriends(myFriendsAcceptedList));
		}
		
		return UserConverter.convertUserListIntoUserDtoList(users, dozerBeanMapper);
	}

	private List<Friend> getMyFriendRequestsList(String userId) {
		return friendsRepository.getMyFriendRequestsList(Long.parseLong(userId));
	}

	private List<User> getUsersFromListOfFriends(List<Friend> friends) {

		List<User> users = new ArrayList<>();
		for (Friend friend : friends) {
			if (friend != null) {
				User user = userService.getUserById(friend.getSender_Id());
				if (user != null) {
					user.setFriendId(friend.getId());
					users.add(user);
				}
			}
		}
		return users;
	}

	private List<User> getUsersFromFriendsList(List<Friend> friends) {

		List<User> users = new ArrayList<>();
		for (Friend friend : friends) {
			if (friend != null) {
				User user = userService.getUserById(friend.getFriend_Id());
				if (user != null) {
					user.setFriendId(friend.getId());
					users.add(user);
				}
			}
		}
		return users;
	}

	@Override
	@Transactional
	public ResponseStatus unFriend(String userId, String friendId) {
		ResponseStatus responseStatus = new ResponseStatus();
		Friend friend = friendsRepository.unFriend(Long.parseLong(userId), Long.parseLong(friendId));

		if (friend != null) {
			friendsRepository.delete(friend);
			responseStatus.setStatus("success");
			return responseStatus;
		} else {
			Friend unFriend = friendsRepository.unFriend(Long.parseLong(friendId), Long.parseLong(userId));
			friendsRepository.delete(unFriend);
			responseStatus.setStatus("success");
			return responseStatus;
		}

	}

	@Override
	@Transactional
	public UserDto findFriendLocation(String friendId) {
		User user = userService.getUserById(Long.parseLong(friendId));

		if (user.getLatitude() > 0 && user.getLongitude() > 0) {
			GeoCodeResponse addressFromLatLng = getAddressFromLatLng(user.getLatitude(), user.getLongitude());
			if (addressFromLatLng != null) {
				if ("OK".equals(addressFromLatLng.getStatus())) {
					for (Results results : addressFromLatLng.getResults()) {
						for (AddressComponent addressComponent : results.getAddress_components()) {
							Collection<String> types = addressComponent.getTypes();
							if (types.isEmpty()) {
								continue;
							} else if (types.contains("route")) {
								user.setAddress(addressComponent.getLong_name());
							} else if (types.contains("sublocality")) {
								String address = user.getAddress();
								if (address != null) {
									user.setAddress(address.concat(",").concat(addressComponent.getLong_name()));
								}
							} else if (types.contains("locality")) {
								String address = user.getAddress();
								if (address != null) {
									user.setAddress(address.concat(",").concat(addressComponent.getLong_name()));
								}
							} else if (types.contains("administrative_area_level_2")) {
								// cityDto.setCode(addressComponent.getShort_name());
								user.setCity(addressComponent.getLong_name());
							} else if (types.contains("administrative_area_level_1")) {
								// stateDto.setCode(addressComponent.getShort_name());
								user.setState(addressComponent.getLong_name());
							} else if (types.contains("country")) {
								// countryDto.setCode(addressComponent.getShort_name());
								user.setCountry(addressComponent.getLong_name());
							} else if (types.contains("postal_code")) { // TODO
								// PostalCode postalCode = new PostalCode();
								// postalCode.setPostalCode(addressComponent.getLong_name());
							}
						}
						break;
					}
				}
				userService.saveUser(user);
			}
		}

		return UserConverter.convertUserIntoUserDto(user, dozerBeanMapper);
	}

	private GeoCodeResponse getAddressFromLatLng(double latitude, double longitude) {

		String latitudeString = String.valueOf(latitude);
		String longitudeString = String.valueOf(longitude);
		String URL = "http://maps.google.com/maps/api/geocode/json?latlng=" + latitudeString + "," + longitudeString + "&sensor=true";

		RestTemplate restTemplate = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		String forObject = restTemplate.getForObject(URL, String.class);
		System.out.println(forObject);
		GeoCodeResponse geoCodeResponse = null;
		try {
			geoCodeResponse = mapper.readValue(forObject, GeoCodeResponse.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return geoCodeResponse;

	}

	@Override
	@Transactional
	public ResponseStatus setFriendRequestStatus(AddFriendDto addFriendDto) {
		Friend friend = friendsRepository.findOne(addFriendDto.getId());
		friend.setAccepted(addFriendDto.isAccepted());
		friend.setRejected(addFriendDto.isRejected());
		ResponseStatus responseStatus = new ResponseStatus();
		friendsRepository.save(friend);
		responseStatus.setStatus("success");
		return responseStatus;
	}
	
	@Override
	@Transactional
	public boolean isAlreadyFriendsBySender(Long senderId, Long recieverId) {

		if (senderId != null && recieverId != null) {
			return friendsRepository.isAlreadyFriendsBySender(senderId, recieverId);
		}
		return false;
	}
	
	@Override
	@Transactional
	public boolean isAlreadyFriendsByReciever(Long recieverId, Long senderId) {

		if (recieverId != null && senderId != null) {
			return friendsRepository.isAlreadyFriendsByReciever(recieverId, senderId);
		}
		return false;
	}

	@Override
	@Transactional
	public boolean isAlreadyFriendsBySenderAcceptStatus(Long senderId, Long recieverId) {
		if (senderId != null && recieverId != null) {
			return friendsRepository.isAlreadyFriendsBySenderAcceptStatus(senderId, recieverId);
		}
		return false;
	}

	@Override
	@Transactional
	public boolean isAlreadyFriendsByRecieverAcceptStatus(Long recieverId, Long senderId) {
		if (senderId != null && recieverId != null) {
			return friendsRepository.isAlreadyFriendsByRecieverAcceptStatus(recieverId, senderId);
		}
		return false;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(userService, "Userservice should not be empty");
	}

}
