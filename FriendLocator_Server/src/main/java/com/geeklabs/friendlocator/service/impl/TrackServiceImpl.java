package com.geeklabs.friendlocator.service.impl;

import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.friendlocator.domain.User;
import com.geeklabs.friendlocator.dto.TrackDto;
import com.geeklabs.friendlocator.service.TrackService;
import com.geeklabs.friendlocator.service.UserService;
import com.geeklabs.friendlocator.util.ResponseStatus;

public class TrackServiceImpl implements TrackService {

	private UserService userService;
	
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	@Override
	@Transactional
	public ResponseStatus saveTrack(String userId, TrackDto trackDto) {
		ResponseStatus responseStatus = new ResponseStatus();
		User user = userService.getUserById(Long.parseLong(userId));
		if (trackDto.getLatitude() > 0 && trackDto.getLongitude() > 0) {
		user.setLatitude(trackDto.getLatitude());
		user.setLongitude(trackDto.getLongitude());
		user.setTrackTime(trackDto.getTrackTime());
		}
		userService.saveUser(user);
		responseStatus.setStatus("success");
		return responseStatus;
	}
}
