package com.geeklabs.friendlocator.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.friendlocator.domain.User;
import com.geeklabs.friendlocator.dto.UserDto;
import com.geeklabs.friendlocator.repository.UserRepository;
import com.geeklabs.friendlocator.service.EmailService;
import com.geeklabs.friendlocator.service.UserService;
import com.geeklabs.friendlocator.util.ResponseStatus;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;

public class UserServiceImpl implements UserService {
	
	private static HttpTransport httpTransport = new NetHttpTransport();
	private static JacksonFactory jacksonFactory = new JacksonFactory();
	
	private UserRepository userRepository;
	
	private DozerBeanMapper dozerBeanMapper;
	
	private EmailService emailService;
	
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	public void setDozerBeanMapper(DozerBeanMapper dozerBeanMapper) {
		this.dozerBeanMapper = dozerBeanMapper;
	}

	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}
	
	@Override
	@Transactional
	public ResponseStatus registerUser(UserDto userDto) {
		com.google.api.services.oauth2.model.Userinfo userInfo = getUserTokenInfo(userDto.getAccessToken());
		
		ResponseStatus responseStatus = new ResponseStatus();
		User user = getUserByEmail(userInfo.getEmail());
		
		if (user != null) {
			
			if (!user.isSignIn()) {
				user.setEmail(userInfo.getEmail());
				user.setName(userInfo.getName());
				user.setPicUrl(userInfo.getPicture());
				user.setAccessToken(userDto.getAccessToken());
				user.setRegisteredDate(new Date());
				user.setSignIn(true);
				userDto.setPicUrl(userInfo.getPicture());
				
				userRepository.save(user);
				responseStatus.setStatus("sign in");
				responseStatus.setId(user.getId());
				responseStatus.setUserEmail(user.getEmail());
				responseStatus.setUserName(user.getName());
				return responseStatus;
			}
			responseStatus.setStatus("success");
			responseStatus.setId(user.getId());
			responseStatus.setUserEmail(user.getEmail());
			responseStatus.setUserName(user.getName());
			return responseStatus;
		} else {
			user = new User();
			user.setEmail(userInfo.getEmail());
			user.setName(userInfo.getName());
			user.setPicUrl(userInfo.getPicture());
			user.setAccessToken(userDto.getAccessToken());
			user.setRegisteredDate(new Date());
			user.setSignIn(true);
			
			userDto.setPicUrl(userInfo.getPicture());
			userDto.setEmail(userInfo.getEmail());
			userDto.setName(userInfo.getName());
			
			emailService.sendWelcomeMail(userDto);
			
			userRepository.save(user);
			responseStatus.setStatus("success");
			responseStatus.setId(user.getId());
			responseStatus.setUserEmail(userInfo.getEmail());
			
			return responseStatus;
		}
	}

	private com.google.api.services.oauth2.model.Userinfo getUserTokenInfo(String accessToken) {
		GoogleCredential googleCredential = new GoogleCredential().setAccessToken(accessToken);
		Oauth2 oauth2 = new Oauth2.Builder(httpTransport, jacksonFactory, googleCredential).build();
		com.google.api.services.oauth2.model.Userinfo userinfo = null;
		try {
			userinfo = oauth2.userinfo().get().execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return userinfo;
	}

	@Override
	@Transactional(readOnly = true)
	public List<UserDto> getAllUsers() {
		Iterable<User> iterable = userRepository.findAll();
		Iterator<User> iterator = iterable.iterator();
		
		List<User> users = new ArrayList<User>();
		
		while (iterator.hasNext()) {
			users.add(iterator.next());
		}
		return convertUserListIntoUserDtoList(users);
	}

	private List<UserDto> convertUserListIntoUserDtoList(List<User> users) {
		List<UserDto> userDtos = new ArrayList<>();
		for (User user : users) {
			UserDto userDto = dozerBeanMapper.map(user, UserDto.class);
			userDtos.add(userDto);
		}
		return userDtos;
	}
	
	@Override
	@Transactional
	public ResponseStatus signOut(String id) {
		User user = userRepository.findOne(Long.parseLong(id));
		user.setSignIn(false);
		userRepository.save(user);
		
		ResponseStatus responseStatus = new ResponseStatus();
		responseStatus.setStatus("SignOut");
		return responseStatus;
	}
	
	@Override
	@Transactional(readOnly = true)
	public User getUserById(long id) {
		return userRepository.findOne(id);
	}
	
	@Override
	@Transactional(readOnly = true)
	public User getUserByEmail(String email) {
		return userRepository.getUserByEmail(email);
	}
	
	@Override
	@Transactional
	public void saveUser(User user) {
		userRepository.save(user);
	}
}
