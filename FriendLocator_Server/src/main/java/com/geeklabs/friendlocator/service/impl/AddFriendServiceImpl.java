package com.geeklabs.friendlocator.service.impl;

import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.friendlocator.domain.Friend;
import com.geeklabs.friendlocator.domain.User;
import com.geeklabs.friendlocator.dto.AddFriendDto;
import com.geeklabs.friendlocator.service.AddFriendService;
import com.geeklabs.friendlocator.service.FriendService;
import com.geeklabs.friendlocator.service.UserService;
import com.geeklabs.friendlocator.util.ResponseStatus;

public class AddFriendServiceImpl implements AddFriendService {

	private UserService userService;
	private FriendService friendsService;

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public void setFriendsService(FriendService friendsService) {
		this.friendsService = friendsService;
	}

	@Override
	@Transactional
	public ResponseStatus addFriendRequest(AddFriendDto addFriendDto, String id) {

		ResponseStatus responseStatus = new ResponseStatus();
		// get User by email and id
		User senderUser = userService.getUserById(Long.parseLong(id));// who sends request
		String reqSenderEmail = senderUser.getEmail();
		Long senderId = senderUser.getId();
		User recievedUser = userService.getUserByEmail(addFriendDto.getEmail());// friend
		Long recieverId = recievedUser.getId();

		if (senderUser != null && !reqSenderEmail.equalsIgnoreCase(addFriendDto.getEmail())) {
			boolean isFriendsBySender = friendsService.isAlreadyFriendsBySender(senderId, recieverId);
			boolean isFriendsByReciever = friendsService.isAlreadyFriendsByReciever(recieverId, senderId);
			boolean isFriendsBySenderAcceptStatus = friendsService.isAlreadyFriendsBySenderAcceptStatus(senderId, recieverId);
			boolean isFriendsByRecieverAcceptStatus = friendsService.isAlreadyFriendsByRecieverAcceptStatus(recieverId, senderId);

			if (!isFriendsBySender && !isFriendsByReciever && !isFriendsBySenderAcceptStatus && !isFriendsByRecieverAcceptStatus) {
				// send a request to friend
				Friend friend = new Friend();

				friend.setFriend_Id(recieverId);
				friend.setSender_Id(senderId);
				friend.setAccepted(false);
				friendsService.save(friend);

			} else {
				responseStatus.setStatus("already_request_sent");
				return responseStatus;
			}

			responseStatus.setStatus("success");

			return responseStatus;
		} else if (reqSenderEmail.equalsIgnoreCase(addFriendDto.getEmail())) {
			responseStatus.setStatus("self req");
			return responseStatus;
		}
		return responseStatus;
	}
}
