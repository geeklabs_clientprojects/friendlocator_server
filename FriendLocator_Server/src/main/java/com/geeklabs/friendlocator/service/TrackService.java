package com.geeklabs.friendlocator.service;

import com.geeklabs.friendlocator.dto.TrackDto;
import com.geeklabs.friendlocator.util.ResponseStatus;

public interface TrackService {

	ResponseStatus saveTrack(String userId, TrackDto trackDto);

}
