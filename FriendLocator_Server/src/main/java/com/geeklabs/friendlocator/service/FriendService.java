package com.geeklabs.friendlocator.service;

import java.util.List;

import com.geeklabs.friendlocator.domain.Friend;
import com.geeklabs.friendlocator.dto.AddFriendDto;
import com.geeklabs.friendlocator.dto.UserDto;
import com.geeklabs.friendlocator.util.ResponseStatus;

public interface FriendService {

	void save(Friend friend);

	List<UserDto> getMyFriends(String userId);

	ResponseStatus unFriend(String userId, String friendId);

	UserDto findFriendLocation(String friendId);

	List<UserDto> getFriendReqs(String userId);
	
	boolean isAlreadyFriendsBySender(Long senderId, Long recieverId);
	
	boolean isAlreadyFriendsByReciever(Long recieverId, Long senderId);
	
	ResponseStatus setFriendRequestStatus(AddFriendDto addFriendDto);

	boolean isAlreadyFriendsBySenderAcceptStatus(Long senderId, Long recieverId);

	boolean isAlreadyFriendsByRecieverAcceptStatus(Long recieverId, Long senderId);



}
