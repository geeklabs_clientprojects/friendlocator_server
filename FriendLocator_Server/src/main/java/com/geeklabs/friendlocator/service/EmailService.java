package com.geeklabs.friendlocator.service;

import com.geeklabs.friendlocator.dto.UserDto;

public interface EmailService {

	void sendWelcomeMail(UserDto userDto);
	
	void send(String toEmail, String subject, String body);
}
