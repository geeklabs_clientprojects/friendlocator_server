package com.geeklabs.friendlocator.service;

import java.util.List;

import com.geeklabs.friendlocator.domain.User;
import com.geeklabs.friendlocator.dto.UserDto;
import com.geeklabs.friendlocator.util.ResponseStatus;

public interface UserService {

	ResponseStatus registerUser(UserDto userDto);

	List<UserDto> getAllUsers();

	ResponseStatus signOut(String id);

	User getUserById(long userId);
	
	User getUserByEmail(String email);
	
	void saveUser(User user);

}

